import { sum, multiplay, sumWithdelay } from "../src/calc";
import { describe } from "mocha";
import { expect } from "chai";
import { resolve } from "path/posix";

describe("calc module", () => {
    context("sum", () => {
        it("should exist", () => {
            expect(sum).to.be.a("Function");
        });
        it("should sum 2 nums", () => {
            const result=sum(1,2);
            expect(result).to.equal(3);
        });
        it("should sum more then 2 nums", () => {
            const result=sum(1,2,3,4,5);
            expect(result).to.equal(15);
        });
    });

    context("multiplay", () => {

        const delay=(ms:number)=>new Promise((resolve)=>setTimeout(resolve,ms));
        it("should exist", () => {
            expect(multiplay).to.be.a("Function");
        });
        it("should multiplay 2 nums", () => {
            const result=multiplay(3,2);
            expect(result).to.eql([6]);
        });
        it("should multiplay more then 2 nums", () => {
            const result=multiplay(5,2,3,4);
            expect(result).to.eql([10,15,20]);
        });

        it("work with", () => {
            delay(1000);
            const result=sum(1,2,3,4,5);
            expect(result).to.equal(15);
        });
    });
    
});
