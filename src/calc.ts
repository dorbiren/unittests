export function sum(...nums:number[]){
    return nums.reduce((acc,n)=>{
        return acc+n;
    },0);
}

export function multiplay(m:number,...nums:number[]){
    return nums.map((n)=>n*m);
}

